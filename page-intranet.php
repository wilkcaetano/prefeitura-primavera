<?php
    get_header();
?>

<div data-aos="fade-up" data-aos-delay="250" class="col-md-12" id="container_principal">
    <div class="container" style="min-height:500px;padding:10px;overflow:hidden;">
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    Contracheque
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="fa fa-shopping-basket"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    Compras Web
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="fa fa-file-text"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    Protocolo
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="glyphicon glyphicon-briefcase"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    Controle Interno
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="fa fa-home"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    Patrimônio
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="fa fa-comment"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    e-Sic
                </div>
            </div>
        </a>
        <a href="">
            <div class="col-md-2" id="caixa-icom">
                <div class="caixa-circulo">
                    <div class="circulo" data-bs-hover-animate="pulse">
                        <span class="glyphicon glyphicon-list-alt"></span>
                    </div>
                </div>
                <div class="caixa-titulo-icones">
                    SCPI 9.0
                </div>
            </div>
        </a>
    </div>
</div>
<?php
    get_footer();
?>
