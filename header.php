<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?php bloginfo('name'); echo " | "; bloginfo('description'); ?></title>

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">


    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CLato:300,400" rel="stylesheet">
    <link rel = "stylesheet" href = "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css"/>

    <!-- Owl Carousel -->
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/owl.carousel.css" />
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>css/owl.theme.default.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url');?>/css/style.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <style>
        .screen-reader-text{
            display: none;
        }
    </style>


    <?php wp_head();?>
</head>
<body>

<!-- Header -->
<header id="header">
    <!-- Top Header -->
    <div id="top-header"  style="background: #2C57A3;">
        <div class="row" style="margin: 0px;">
            <div class="header-links" id="acessibilidade">
                <ul>
                    <li><a href="#conteudo" accesskey="1">Ir Para Conteudo (Alt + 1)</a></li>
                    <li><a href="#menu" accesskey="2">Ir Para Menu (Alt + 2)</a></li>
                    <li onclick="foco()"><a href="#busca" accesskey="3">Ir Para Pesquisa (Alt + 3)</a></li>
                    <li><a href="#footer" accesskey="4">Ir Para Rodapé (Alt + 4)</a></li>
                    <li><a href="mapa-do-site"><i class="fa fa-map"></i> Mapa do Site</a></li>
                </ul>
            </div>
            <div class="header-social">
                <ul>
                    <li><a href="https://www.facebook.com/prefeituradeprimaverape/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="http://www.vlibras.gov.br/"><i class="fa fa-sign-language"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Top Header -->

    <!-- Center Header -->
        <div id="center-header">
            <div class="col-md-12" id="baixar"></div>
            <div class="container" style="padding: 0px;">
                <div class="col-md-5 col-sm-12 col-xs-12 header-logo" id="img-logo" style="padding-top: 15px;">
                    <a href="<?php bloginfo('url'); ?>">
                        <?php query_posts('post_type=logomarca&post_per_page=1')?>
                        <?php if(have_posts()): ?>
                        <?php while(have_posts()): the_post();?>
                                <img src="<?php the_field('imagens');?>" alt="" style="width: 350px;margin: 0 auto;">
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </a>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12" style="position:relative; float:right;">
                    <div class="widget social-widget" id="cabecalho">
                        <ul>
                            <?php query_posts('post_type=icones_inst&post_per_page=1&order=ASC')?>
                            <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post();?>
                            <li><a href="<?php the_field('link');?>" class="<?php the_field('classe');?>"><i class="fa <?php the_field('icon');?>"></i><br><span><?php the_field('titulo');?></span></a></li>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- /Center Header -->

    <!-- Nav Header -->
    <div id="nav-header">
        <div class="container" style="padding-top: 5px;overflow: hidden;min-height: 50px;">
            <nav id="main-nav">
                <div class="nav-logo">
                    <a href="#" class="logo"><img src="./img/logo-branca.png" alt=""></a>
                </div>
                <div class="main-nav nav navbar-nav" id="menu">
                    <?php
                    wp_nav_menu(
                        array('theme_location' => 'meu_menu_principal')
                    );

                    ?>
                </div>
            </nav>
            <div class="button-nav" id="busca">
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
    <!-- /Nav Header -->
</header>
<div id="conteudo"></div>