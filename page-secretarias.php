<?php $url = $_GET['sec'];
if ($url == ''){
    the_content();
}
?>

<?php query_posts('post_type=secretarias') ?>
<?php if(have_posts()): ?>
    <?php while(have_posts()): the_post();?>
        <?php $post_slug = get_post_field( 'post_name', get_post() );

        if ($post_slug == $url) {
            ?>
            <div class="col-md-12" style="text-align: center; font-family: 'Arial Black'; font-size: 25px;border-bottom: 1px solid #edecec;margin-bottom: 20px;">
                <?php the_title(); ?>
            </div>
            <ul class="nav nav-tabs" id="rowTab">
                <li class="active"><a href="#personal-info" data-toggle="tab">Secretariado</a></li>
                <li><a href="#Employment-info" data-toggle="tab">Como Chegar ?</a></li>
                <li><a href="#career-path" data-toggle="tab">Informativo</a></li>
            </ul>
            <!-- end: tabs link -->

            <div class="tab-content" style="margin-bottom: 40px;">
                <div class="tab-pane active" id="personal-info">
                    <?php the_field('conteudo'); ?>
                </div>

                <div class="tab-pane" id="Employment-info">
                    <?php the_field('endereco'); ?>
                </div>

                <div class="tab-pane" id="career-path">
                    <?php the_field('informativos'); ?>
                </div>
            </div>

            <?php
        }
        ?>
    <?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>

