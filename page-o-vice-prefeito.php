<?php
    /*
     * /*
 * template name: A Prefeita
 * */
     get_header();

?>

<div class="container" style="min-height: 450px;margin-top: 20px;padding: 0px;">
    <div class="col-md-12" id="titulo-parlamentar">
        <?php the_title(); ?>
    </div>
    <div class="col-md-12" id="cab-perfil" style="box-shadow: 0px 2px rgba(0, 0, 0, 0.2);"">
        <div class="col-md-8" style="padding: 0px;">
            <div class="col-md-3" style="padding: 0px;overflow: hidden;">
                <div id="moldura-foto">
                    <div class="fotografia">
                        <?php query_posts('post_type=perfil') ?>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post();?>
                                <?php $post_slug = get_post_field( 'post_name', get_post() );
                                    if ($post_slug == "o-vice-prefeito"){
                                ?>
                                <img src="<?php the_field('foto_perfil'); ?>" alt="" style="width: 100%;" />

                            <?php } endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="col-md-12" id="espaco"></div>
                <div class="nome">
                    <?php query_posts('post_type=perfil') ?>
                    <?php if(have_posts()): ?>
                        <?php while(have_posts()): the_post();?>
                        <?php $post_slug = get_post_field( 'post_name', get_post() );
                                    if ($post_slug == "o-vice-prefeito"){
                                ?>
                        <?php the_field('nome'); ?>
                            <p><?php the_field('abaixo_do_nome'); ?></p>
                        <?php } endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
            <div class="col-md-4 widget social-widget" style="text-align: center;">
                <div class="col-md-12" id="espaco"></div>
                <ul>
                    <li>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    <div class="col-md-4" style="border-left: 1px solid lightgrey;height: 180px;margin-top: 10px;color: #A340A6;">
        <div class="col-md-12" id="espaco"></div>
        <p>
            <?php query_posts('post_type=perfil') ?>
            <?php if(have_posts()): ?>
            <?php while(have_posts()): the_post();?>
            <?php $post_slug = get_post_field( 'post_name', get_post() );
            if ($post_slug == "o-vice-prefeito"){
            ?>
            <?php the_field('texto_lado_direito'); ?>
        <?php } endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        </p>
    </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px; margin-bottom: 20px;min-height: 250px; background: #edecec;">
        <div class="row content-panel">
            <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active">
                        <a data-toggle="tab" href="#overview">Biografia</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#contact" class="contact-map">Agenda</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#edit">Galeria</a>
                    </li>
                </ul>
            </div>
            <!-- /panel-heading -->
            <div class="panel-body">
                <div class="tab-content">
                    <div id="overview" class="tab-pane active">
                        <div class="row" style="padding: 15px;">
                            <?php query_posts('post_type=perfil') ?>
                            <?php if(have_posts()): ?>
                                <?php while(have_posts()): the_post();?>
                                    <?php $post_slug = get_post_field( 'post_name', get_post() );
                                    if ($post_slug == "o-vice-prefeito"){
                                        ?>
                                        <?php the_field('biografia'); ?>
                                    <?php } endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <!-- /OVERVIEW -->
                    </div>
                    <!-- /tab-pane -->
                    <div id="contact" class="tab-pane">
                        <div class="row" style="padding: 10px;">
                            <?php query_posts('post_type=perfil') ?>
                            <?php if(have_posts()): ?>
                                <?php while(have_posts()): the_post();?>
                                    <?php $post_slug = get_post_field( 'post_name', get_post() );
                                    if ($post_slug == "o-vice-prefeito"){
                                        ?>
                                        <?php the_field('agenda'); ?>
                                    <?php } endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /tab-pane -->
                    <div id="edit" class="tab-pane">
                        <div class="row" style="padding: 10px;">
                            <?php query_posts('post_type=perfil') ?>
                            <?php if(have_posts()): ?>
                                <?php while(have_posts()): the_post();?>
                                    <?php $post_slug = get_post_field( 'post_name', get_post() );
                                    if ($post_slug == "o-vice-prefeito"){
                                        ?>
                                        <?php the_field('galeria'); ?>
                                    <?php } endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <!-- /row -->
                    </div>
                    <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
            </div>
            <!-- /panel-body -->
        </div>
    </div>
</div>


<?php
    get_footer();
?>
