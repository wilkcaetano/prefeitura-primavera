<footer id="footer" style="width: 100%;">
    <!-- Top Footer -->
    <div id="top-footer" class="section">
        <!-- CONTAINER -->
        <div class="container">
            <!-- ROW -->
            <div class="row">
                <!-- Column 1 -->
                <div class="col-md-4" id="caixa01" style="margin-bottom: 10px;">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="titulo-menu-sec" style="color:white;font-family: 'Arial Black';border: 1px solid white;padding:5px;width:70%;">
                            Secretarias
                        </div>
                        <?php query_posts('post_type=secretarias') ?>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post();?>
                                <?php $post_slug = get_post_field( 'post_name', get_post() );?>
                                <?php $post_id = get_the_ID();?>
                            <ul style="margin-top: 10px; color: white;">
                                <li><a href="index.php/secretaria/?sec=<?php echo $post_slug; ?>"><?php the_title(); ?></a></li>
                            </ul>

                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
                <!-- /Column 1 -->

                <!-- Column 2 -->
                <div class="col-md-4" id="caixa02">
                    <div class="col-md-12" style="padding: 0px;">
                        <div class="titulo-menu-sec" style="color:white;font-family: 'Arial Black';border: 1px solid white;padding:5px;width:70%;">
                            Menu
                        </div>
                    <?php
                    wp_nav_menu(
                        array('theme_location' => 'menu_rodape')
                    );

                    ?>
                    </div>
                </div>
                <!-- /Column 2 -->

                <!-- Column 3 -->
                <div class="col-md-4" id="caixa03">
                    <!-- footer galery -->

                    <!-- /footer tweets -->
                </div>
                <!-- /Column 3 -->
            </div>
            <!-- /ROW -->
        </div>
        <!-- /CONTAINER -->
    </div>
    <!-- /Top Footer -->

    <!-- Bottom Footer -->
    <div id="bottom-footer" class="section">
        <!-- CONTAINER -->
        <div class="container">
            <!-- ROW -->
            <div class="row">
                <!-- footer links -->
                <div class="col-md-6 col-md-push-6">
                    <!--<ul class="footer-links">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Advertisement</a></li>
                        <li><a href="#">Privacy</a></li>
                    </ul>-->
                </div>
                <!-- /footer links -->

                <!-- footer copyright -->
                <div class="col-md-6 col-md-pull-6">
                    <div class="footer-copyright">
								<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados a Prefeitura Municipal de Primavera Pernambuco  by <a href="https://prodatta.com" target="_blank">Prodatta</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
                    </div>
                </div>
                <!-- /footer copyright -->
            </div>
            <!-- /ROW -->
        </div>
        <!-- /CONTAINER -->
    </div>
    <!-- /Bottom Footer -->
</footer>
<!-- /FOOTER -->

<!-- Back to top -->
<div id="back-to-top"></div>
<!-- Back to top -->
<!-- jQuery Plugins -->
<script src="<?php bloginfo('template_url');?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/main.js"></script>
<script src="<?php bloginfo('template_url');?>/js/acessibilidade.js"></script>
<script>
    function foco(){
        $("input:text:eq(0):visible").focus();
    }
</script>
<?php wp_footer();?>
</body>
</html>

