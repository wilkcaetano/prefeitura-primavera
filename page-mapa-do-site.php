<?php
    get_header();
?>
<div class="container" style="overflow: hidden;eight: 650px; padding-top: 100px;margin-bottom: 100px">
<div class="sitemap">

    <nav class="primaryNav">
        <ul>
            <li id="home"><a href="">Menu</a></li>
            <li><a href="">Município</a>
                <ul>
                    <li><a href="../historia-do-municipio">História do Município</a></li>
                    <li><a href="../dados-geograficos">Dados Geográficos</a></li>
                    <li><a href="../hino-do-municipio">Hino do Município</a></li>
                    <li><a href="../cachoeira-do-urubu">Cachoeira do Urubu</a></li>
                    <li><a href="../simbolos">Símbolos</a></li>
                    <li><a href="../feriados-municipais">Feriados Municipais</a></li>
                    <li><a href="../turismo">Turismo</a></li>
                </ul>
            </li>

            <li><a href="">Governo</a>
                <ul>
                    <li><a href="../a-prefeitura">A Prefeitura</a></li>
                    <ul>
                        <li><a href="../a-prefeita">A Prefeita</a></li>
                        <li><a href="../o-vice-prefeito">O Vice-Prefeito</a></li>
                    </ul>
                </ul>
                <ul>
                    <li><a href="../secretaria">Secretarias</a></li>
                    <li><a href="../procuradoria-municipal">Procuradoria Municipal</a></li>
                    <li><a href="../organograma">Organograma</a></li>
                </ul>
            </li>
            <li><a href="">Legislação</a>
                <ul>
                    <li><a href="../codigo-tributario/">Código Tributário</a></li>
                    <li><a href="../lei-organica-municipal/">Lei Orgânica Municipal</a></li>
                    <li><a href="../regulamentacao-lai/">Regulamentação LAI</a></li>
                    <li><a href="../estatuto-do-servidor/">Estatuto do Servidor</a></li>
                </ul>
            </li>
            <li><a href="">Contatos</a>
                <ul>
                    <li><a href="">Contato Principal</a>
                        <ul>
                            <li><a href="../contatos">Formulário de Contato</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="">Intranet</a>
                <ul>
                    <li><a href="../intranet">Icones da Página Intranet</a>
                    </li>
                </ul>
            </li>
        </ul>

    </nav>

</div>
</div>
<?php
    get_footer();
?>
