<button class="nav-collapse-btn"><i class="fa fa-bars"></i></button>
<button class="search-collapse-btn" style="height: 50px;padding: 10px;padding-right: 10px;"><i class="fa fa-search"></i></button>
<div class="search-form" style="z-index: 9999;position: relative; float:left;height: 50px;padding-top: 5px;margin-bottom: 5px; background: none; border:none;">
    <form action="<?php echo home_url( '/' ); ?>" method="get" accept-charset="utf-8" id="searchform" role="search">
        <input class="input" type="text" name="s" placeholder="<?php echo esc_attr_x( 'Pesquisar no site …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>">
    </form>
</div>