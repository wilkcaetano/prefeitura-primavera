<!-- Owl Carousel 1 -->
<div id="owl-carousel-1" class="owl-carousel owl-theme center-owl-nav">

    <?php query_posts('category_name=Noticias'); ?>
    <?php
    if(have_posts()) : while(have_posts()) : the_post();
    ?>
    <!-- ARTICLE -->
    <article class="article thumb-article">
        <div class="article-img" style="min-height: 570px;overflow: hidden;">
            <?php the_post_thumbnail(array(760, 570)); ?>
        </div>
        <div class="article-body">
            <ul class="article-info">
                <li class="article-category">
                    <a href="#"><?php single_cat_title(); ?></a>
                </li>
                <li class="article-type"><i class="fa fa-camera"></i></li>
            </ul>
            <h2 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <ul class="article-meta">
                <li><i class="fa fa-clock-o"></i>&nbsp;  <?php the_date("d/M/Y"); ?> </li>
                <li><i class="fa fa-comments"></i>&nbsp; <?php comments_number("Nenhum Comentário","1 Comentário","% Comentário"); ?> </li>
            </ul>
        </div>
    </article>
    <!-- /ARTICLE -->
    <?php
    endwhile;
    else:
        ?>
        <p>Nenhum Post Encontrado!</p>

    <?php
    endif;
    ?>
    <?php wp_reset_query(); ?>

</div>
<!-- /Owl Carousel 1 -->
<div class="section">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- Main Column -->
            <div class="col-md-8">
                <!-- row -->
                <div class="row">
                    <!-- Column 1 -->
                    <?php query_posts('category_name=Saúde&showposts=1'); ?>
                    <?php if(have_posts()): while(have_posts()): the_post(); ?>
                        <div class="col-md-6 col-sm-6">
                            <!-- section title -->
                            <div class="section-title">
                                <h2 class="title"><?php single_cat_title(); ?></h2>
                            </div>
                            <!-- /section title -->

                            <!-- ARTICLE -->
                            <article class="article">
                                <div class="article-img">
                                    <a href="#">
                                        <?php the_post_thumbnail(array(9999, 240)); ?>
                                    </a>
                                    <ul class="article-info">
                                        <li class="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </div>
                                <div class="article-body">
                                    <h3 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <ul class="article-meta">
                                        <li><i class="fa fa-clock-o"></i> <?php the_date("d/M/Y"); ?></li>
                                        <li><i class="fa fa-comments"></i> <?php comments_number("Nenhum Comentário","1 Comentário","% Comentário"); ?></li>
                                    </ul>
                                    <p><?php echo excerpt(30); ?></p>
                                </div>
                            </article>
                        </div>
                    <?php endwhile; ?>
                    <?php else: ?>
                        Não Consta posts nessa categoria
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <!-- /Column 1 -->

                    <!-- Column 2 -->
                    <?php query_posts('category_name=Noticias&showposts=1&order=ASC'); ?>
                    <?php if(have_posts()): while(have_posts()): the_post(); ?>
                        <div class="col-md-6 col-sm-6">
                            <!-- section title -->
                            <div class="section-title">
                                <h2 class="title"><?php single_cat_title(); ?></h2>
                            </div>
                            <!-- /section title -->

                            <!-- ARTICLE -->
                            <article class="article">
                                <div class="article-img" style="overflow: hidden;">
                                    <a href="#">
                                        <?php the_post_thumbnail(array(9999, 240)); ?>
                                    </a>
                                    <ul class="article-info">
                                        <li class="article-type"><i class="fa fa-camera"></i></li>
                                    </ul>
                                </div>
                                <div class="article-body">
                                    <h3 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <ul class="article-meta">
                                        <li><i class="fa fa-clock-o"></i> <?php the_date("d/M/Y"); ?></li>
                                        <li><i class="fa fa-comments"></i> <?php comments_number("Nenhum Comentário","1 Comentário","% Comentário"); ?></li>
                                    </ul>
                                    <p><?php echo excerpt(30); ?></p>
                                </div>
                            </article>
                        </div>
                    <?php endwhile; ?>
                    <?php else: ?>
                        Não Consta posts nessa categoria
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <!-- /Column 2 -->
                </div>
                <!-- /row -->

                <!-- row -->


                <!-- row -->

                <!-- /row -->
            </div>
            <!-- /Main Column -->

            <!-- Aside Column -->
            <div class="col-md-4">
                <div class="widget social-widget">
                    <div class="section-title">
                        <h2 class="title">Acesso a Informação</h2>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="position: relative;float: left;margin-bottom: 20px;">
                        <img src="http://prodatta.inf.br/prefeituraprimavera/wp-content/uploads/2019/02/logo-e-sic.png" alt="" style="width: 250px;margin-left: 15%;">
                        <div class="col-md-6" style="height:30px;padding: 0px;border-right:1px solid white;">
                            <button style="width:100%; height: 100%; background: green;color: white;border: none;margin-top: 10px;">Abrir Chamado</button>
                        </div>
                        <div class="col-md-6" style="padding: 0px;height:30px;">
                            <button style="width:100%; height: 100%; background: #FBC249;color: white;border: none;margin-top: 10px;">Consultar Chamado</button>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <div class="col-md-12" style="font-size: 20px; text-align: center;text-transform: uppercase; font-family: 'Arial Black'; color: rgba(0, 168, 89, 0.5);">
                            Sic Presencial
                        </div>
                        <div class="col-md-12" id="sic-p" style="margin-top: 10px;">
                            <p>Endereço: Rua Coronel Braz Cavalcante 42, Primavera - PE, 55510-000</p>
                            <p>Atendimento ⋅ 7h às 13H</p>
                            <p>Telefone: (81) 3562-1126</p>
                            <p>Responsável: </p>
                        </div>
                    </div>
                    <!--<div class="col-md-12" style="height: 60px; background: green; text-align: center;line-height: 60px;font-size: 26px;color: white;margin-bottom: 20px;">
                        <i class="fa fa-search" style="position: relative; float: left;line-height: 60px;font-size: 30px;margin-right: 10px;"></i>
                        <p style="position: relative; float: left;">Portal da Transparência</p>
                    </div>-->
                </div>

                <!-- social widget -->

                <div class="widget social-widget" id="icones-info" style="position: relative; float: left;margin-bottom: 100px;">
                    <div class="section-title">
                        <h2 class="title">Acesse</h2>
                    </div>
                    <ul>
                        <?php query_posts('post_type=icones_inst&post_per_page=1&order=ASC')?>
                        <?php if(have_posts()): ?>
                        <?php while(have_posts()): the_post();?>
                                <li style="width: 100%;"><a href="<?php the_field('link');?>" class="<?php the_field('classe');?>"><i class="fa <?php the_field('icon');?>"></i><br><span><?php the_field('titulo');?></span></a></li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </ul>
                </div>
                <div class="widget social-widget" style="position: relative; float: left; width: 100%;">
                    <div class="section-title">
                        <h2 class="title">Governo Municipal</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="select" data-id="Governo Municipal">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="menu-secretarias-container">
                                        <form>
                                            <select class="form-control" data-size="15" onchange="if (this.value) window.location.href=this.value">
                                                <option value="">Selecione a Secretaria</option>
                                                <?php query_posts('post_type=secretarias') ?>
                                                <?php if(have_posts()): ?>
                                                    <?php while(have_posts()): the_post();?>
                                                        <?php $post_slug = get_post_field( 'post_name', get_post() );?>
                                                        <?php $post_id = get_the_ID();?>
                                                        <option value="index.php/secretaria/?sec=<?php echo $post_slug;?>"><?php the_title(); ?></option>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                                <?php wp_reset_query(); ?>
                                            </select>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /ARTICLE -->
            </div>
            <!-- /article widget -->
        </div>
        <!-- /Aside Column -->
    </div>
    <!-- /ROW -->
</div>
<!-- /CONTAINER -->
</div>

<!-- SECTION -->
<div class="section">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- Main Column -->
            <div class="col-md-12">
                <!-- section title -->
                <div class="section-title">
                    <h2 class="title">Categorias</h2>
                    <!-- tab nav -->
                    <ul class="tab-nav pull-right">
                        <li class="active"><a data-toggle="tab" href="#tab" class="link" > Todas</a></li>
                        <?php
                        foreach (get_categories() as $category) {
                        ?>
                            <li onclick='hover()' ><a data-toggle='tab' href='#tab<?php echo $category->term_id; ?>' id='link'><?php echo $category->name; ?></a></li>

                        <?php

                        }
                        ?>

                        <!--
                        <li><a data-toggle="tab" href="#tab1">News</a></li>
                        <li><a data-toggle="tab" href="#tab1">Sport</a></li>
                        <li><a data-toggle="tab" href="#tab1">Music</a></li>
                        <li><a data-toggle="tab" href="#tab1">Business</a></li>
                        <li><a data-toggle="tab" href="#tab1">Lifestyle</a></li>
                        -->


                    </ul>
                    <!-- /tab nav -->
                </div>

                <!-- /section title -->
                <!-- Tab content -->
                <div class="tab-content">
                    <!-- tab1 -->
                    <div id="tab" class="tab-pane fade in active">
                        <!-- row -->
                        <div class="row">
                            <!-- Column 1 -->
                            <?php  query_posts('showposts=4&amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;orderby=rand'); ?>
                            <?php
                            if(have_posts()) : while(have_posts()) : the_post();
                            ?>
                            <div class="col-md-3 col-sm-6">
                                <!-- ARTICLE -->
                                <article class="article">
                                    <div class="article-img" style="overflow: hidden;">
                                        <a href="#">
                                            <?php the_post_thumbnail(array(9999, 197)); ?>
                                        </a>
                                        <ul class="article-info">
                                            <li class="article-type"><i class="fa fa-camera"></i></li>
                                        </ul>
                                    </div>
                                    <div class="article-body">
                                        <h4 class="article-title"><a href="<?php the_permalink();?>"><?php title_limite(42); ?></a></h4>
                                        <ul class="article-meta">
                                            <li><i class="fa fa-clock-o"></i> <?php the_date('d/M/Y');?></li>
                                            <li><i class="fa fa-comments"></i> 33</li>
                                        </ul>
                                    </div>
                                </article>
                                <!-- /ARTICLE -->
                            </div>
                            <!-- /Column 1 -->
                            <?php
                            endwhile;
                            else:
                                ?>
                                <p>Nenhum Post Encontrado!</p>

                            <?php
                            endif;
                            ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <!-- /row -->

                        <!-- row -->

                        <!-- /row -->
                    </div>
                    <?php
                    foreach (get_categories() as $category) {

                        ?>

                        <div id="tab<?php echo $category->term_id; ?>" class="tab-pane fade out ">
                            <div class="row">
                                <!-- Column 1 -->
                                <?php
                                global $post;
                                $args = array('numberposts' => 4, 'cat' => $category->term_id);
                                $myposts = get_posts($args);
                                foreach ($myposts as $post) : setup_postdata($post); ?>
                                    <div class="col-md-3 col-sm-6">
                                        <!-- ARTICLE -->
                                        <article class="article">
                                            <div class="article-img" style="overflow: hidden;">
                                                <a href="#">
                                                    <?php the_post_thumbnail(array(9999, 197)); ?>
                                                </a>
                                                <ul class="article-info">
                                                    <li class="article-type"><i class="fa fa-camera"></i></li>
                                                </ul>
                                            </div>
                                            <div class="article-body">
                                                <h4 class="article-title"><a href="<?php the_permalink();?>"><?php title_limite(42); ?></a>
                                                </h4>
                                                <ul class="article-meta">
                                                    <li><i class="fa fa-clock-o"></i><?php the_date('d/M/Y');?></li>
                                                    <li><i class="fa fa-comments"></i> 33</li>
                                                </ul>
                                            </div>
                                        </article>
                                        <!-- /ARTICLE -->
                                    </div>
                                <?php endforeach; ?>
                                <!-- /Column 1 -->

                            </div>
                            <a href="blog/category/<?php echo $category->slug; ?>">
                                <button type="button" class="btn"  style="background:#A340A6;color: white;">Ver Mais</button>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- /tab1 -->
                </div>
                <!-- /tab content -->
            </div>
            <!-- /Main Column -->
        </div>
        <!-- /ROW -->
    </div>
    <!-- /CONTAINER -->
</div>
<!-- /SECTION -->

<!-- SECTION -->

<!-- /SECTION -->



<!-- SECTION -->
<div class="section">
    <!-- CONTAINER -->
    <div class="container">
        <!-- ROW -->
        <div class="row">
            <!-- Main Column -->
            <div class="col-md-12">
                <!-- section title -->
                <div class="section-title">
                    <h2 class="title">Popular Video</h2>
                    <div id="nav-carousel-2" class="custom-owl-nav pull-right"></div>
                </div>
                <!-- /section title -->

                <!-- owl carousel 2 -->
                <div id="owl-carousel-2" class="owl-carousel owl-theme">
                    <!-- ARTICLE -->
                    <?php query_posts('post_type=videos')?>
                    <?php if(have_posts()): ?>
                    <?php while(have_posts()): the_post();?>
                    <article class="article thumb-article" >
                            <?php the_field('iframe');?>
                    </article>
                    <!-- /ARTICLE -->
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>

                </div>
                <!-- /owl carousel 2 -->
            </div>
            <!-- /Main Column -->
        </div>
        <!-- /ROW -->
    </div>
    <!-- /CONTAINER -->

</div>
<!-- /SECTION -->
<div class="section">
    <div class="container">
        <div class="col-md-8">
            <div class="section-title">
                <h2 class="title">Redes Sociais</h2>
            </div>
            <div class="col-md-6" style="height: 100%;">
                <div class="fb-page" data-href="https://www.facebook.com/prefeituradeprimaverape" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/prefeituradeprimaverape" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/prefeituradeprimaverape">Prefeitura Municipal de Primavera</a>
                    </blockquote>
                </div>
            </div>
            <div class="col-md-6">
                <div class="fb-page" data-href="https://www.facebook.com/prefeituradeprimaverape" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/prefeituradeprimaverape" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/prefeituradeprimaverape">Prefeitura Municipal de Primavera</a>
                    </blockquote>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="section-title">
                <h2 class="title">Icones</h2>
            </div>
            <div class="col-md-6">
                <img src="<?php bloginfo('template_url');?>/img/ico02.png" alt="" style="width: 100%;">
            </div>
            <div class="col-md-6">
                <img src="<?php bloginfo('template_url');?>/img/ico03.png" alt="" style="width: 100%;">
            </div>
            <div class="col-md-12" style="margin-top: 30px;padding: 0;">
                <div class="col-md-6">
                    <img src="<?php bloginfo('template_url');?>/img/ico04.png" alt="" style="width: 100%;">
                </div>
                <div class="col-md-6">
                    <img src="<?php bloginfo('template_url');?>/img/ico01.png" alt="" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SECTION -->

<!-- /SECTION -->