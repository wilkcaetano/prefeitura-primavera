<?php
/*
 * template name: Secretarias
 * */

get_header();
?>

<div class="container" style="min-height: 450px;">
    <div class="row" style="margin:0px;">

        <div class="col-md-12" style="margin-top: 20px;padding: 0px;">
            <div class="container-fluid">
                <div class="row" style="margin-top: 20px;padding: 0px;">
                    <div class="col-md-3" style="overflow:hidden;position: relative;float: left;padding: 0px;">
                        <!-- menu -->
                        <div id="MainMenu">
                            <div class="list-group panel">
                                <?php query_posts('post_type=secretarias&orderby=title&order=ASC&showposts=-1') ?>
                                <?php if(have_posts()): ?>
                                    <?php while(have_posts()): the_post();?>
                                        <?php $post_slug = get_post_field( 'post_name', get_post() );?>
                                        <?php $post_id = get_the_ID();?>
                                        <a href="?sec=<?php echo $post_slug; ?>" class="list-group-item list-group-item-info"><?php the_title(); ?> <i class="fa fa-caret-right"></i></a>
                                <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="conteudo-secretarias" style="position: relative; float: left; min-height: 300px;overflow: hidden;">
                        <?php include "page-secretarias.php"; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
