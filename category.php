<?php
    get_header();
?>


<div class="row" style="margin: 0px;margin-bottom: 20px;">
    <div class="container">
        <div class="row" id="trila-de-pao" style="margin:0;padding:0;">
            <div class="container" id="texto-trilha">
                <?php custom_breadcrumbs(); ?>
            </div>
        </div>
        <?php if (have_posts()) : ?>
        <div class="container" style="height:400px;">
            <h2>Posts da  Categoria <?php single_cat_title(); ?></h2>
            <?php /* Start the Loop */ ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-md-3 col-sm-6">
                    <!-- ARTICLE -->
                    <article class="article">
                        <div class="article-img" style="overflow: hidden;">
                            <a href="<?php the_permalink();?>">
                                <?php the_post_thumbnail(array(9999, 197)); ?>
                            </a>
                            <ul class="article-info">
                                <li class="article-type"><i class="fa fa-camera"></i></li>
                            </ul>
                        </div>
                        <div class="article-body">
                            <h4 class="article-title"><a href="<?php the_permalink();?>"><?php title_limite(42); ?></a>
                            </h4>
                            <ul class="article-meta">
                                <li><i class="fa fa-clock-o"></i><?php the_date('d/M/Y');?></li>
                                <li><i class="fa fa-comments"></i> 33</li>
                            </ul>
                        </div>
                    </article>
                    <!-- /ARTICLE -->
                </div>
            <?php endwhile; ?>


            <?php else : ?>

                Nenhuma Categoria cadastrada

            <?php endif; ?>
            <div class="col-md-12">
                <h3>Todas as Categorias</h3>
                <div class="widget-tags">
                    <ul>
                        <?php
                        foreach (get_categories() as $category) {
                            ?>
                            <li><a href="../../../blog/category/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<?php
    get_footer();
?>
