<?php

get_header();
?>

    <!-- CONTAINER -->
    <div class="container" style="margin-bottom: 20px;">
        <!-- ROW -->
        <div class="row">
            <!-- Main Column -->
            <div class="col-md-8">

                <!-- breadcrumb -->
                <ul class="article-breadcrumb">
                    <?php custom_breadcrumbs(); ?>
                </ul>
                <!-- /breadcrumb -->

                <!-- ARTICLE POST -->
                <article class="article article-post" style="overflow: hidden;">
                    <?php if(have_posts()): while(have_posts()): the_post(); ?>
                    <div class="article-body">
                        <h1 class="article-title"><?php the_title(); ?></h1>
                        <ul class="article-meta">
                            <li><i class="fa fa-clock-o"></i> <?php the_date('d/M/Y'); ?></li>
                            <li><i class="fa fa-comments"></i> <?php comments_number("Nenhum Comentário","1 Comentário","% Comentário"); ?></li>
                        </ul>
                        <?php the_content();?>
                    </div>

                </article>
                <!-- /ARTICLE POST -->

                <!-- widget tags -->
                <div class="widget-tags">
                    <ul>
                        <?php
                        foreach (get_pages() as $pages) {
                            ?>
                            <li><a href="<?php echo $pages->post_name; ?>"><?php echo $pages->post_title; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <!-- /widget tags -->

                <!-- article comments -->
                <!-- /article comments -->
                <?php endwhile; ?>
                <?php else: ?>
                    Não á Posts Cadastrados
                <?php endif; ?>
                <!-- reply form -->

                <!-- /reply form -->
            </div>
            <!-- /Main Column -->

            <!-- Aside Column -->
            <div class="col-md-4">
                <!-- Ad widget -->
                <div class="widget center-block hidden-xs">
                    <img class="center-block" src="./img/ad-1.jpg" alt="">
                </div>
                <div class="widget">
                    <div class="section-title">
                        <h2 class="title">Posts Recentes</h2>
                    </div>

                    <!-- owl carousel 3 -->
                    <div id="owl-carousel-3" class="owl-carousel owl-theme center-owl-nav">
                        <!-- ARTICLE -->
                        <?php query_posts('numberposts=5&amp;amp;amp;amp;amp;amp;amp;amp;amp;amp;orderby=rand'); ?>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post();?>
                                <article class="article">
                                    <div class="article-img">
                                        <a href="#">
                                            <?php the_post_thumbnail(array(9999, 270)); ?>
                                        </a>
                                        <ul class="article-info">
                                            <li class="article-type"><i class="fa fa-file-text"></i></li>
                                        </ul>
                                    </div>
                                    <div class="article-body">
                                        <h4 class="article-title"><a href="#"><?php the_title(); ?></a></h4>
                                        <ul class="article-meta">
                                            <li><i class="fa fa-clock-o"></i> January 31, 2017</li>
                                            <li><i class="fa fa-comments"></i> 33</li>
                                        </ul>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                        <!-- /ARTICLE -->

                        <!-- ARTICLE -->
                        <!-- /ARTICLE -->
                    </div>
                    <!-- /article widget -->

                    <!-- article widget -->

                    <!-- /article widget -->
                </div>

                <div class="widget social-widget" style="position: relative; float: left;margin-bottom: 100px;">
                    <div class="section-title">
                        <h2 class="title">Acesse</h2>
                    </div>
                    <ul>
                        <?php query_posts('post_type=icones_inst&post_per_page=1&order=ASC')?>
                        <?php if(have_posts()): ?>
                            <?php while(have_posts()): the_post();?>
                                <li style="width: 100%;"><a href="<?php the_field('link');?>" class="<?php the_field('classe');?>"><i class="fa <?php the_field('icon');?>"></i><br><span><?php the_field('titulo');?></span></a></li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>

                    </ul>
                </div>
                <!-- /Aside Column -->
            </div>
            <!-- /ROW -->
        </div>
        <!-- /CONTAINER -->
    </div>
<?php
get_footer();
?>
